﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;



public class ThingWorx : MonoBehaviour {
    //string url = "http://52.73.185.40/Thingworx/Things/Freezer_Num1/Properties?x-thingworx-session=true&appKey=78bfe229-afc8-475c-ace7-7aea8ed8a6a4";
    string url;
    WWW www;
    public string thingname;
    public GameObject json_Name;
    public GameObject json_Temp;
    public GameObject json_isBelowFreezing;
    public GameObject json_description;
    public GameObject barGraph;
    string outPutThing;
    string HTMLHolder;
    string[,] table;
    

    //ge2-8955.cloud
    // Use this for initialization
    void Start () {
        url = "http://52.73.185.40/Thingworx/Things/" + thingname + "/Properties?x-thingworx-session=true&appKey=78bfe229-afc8-475c-ace7-7aea8ed8a6a4";
        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Accept", "application/json");
        www = new WWW(url, null, headers);
        StartCoroutine(WaitForRequest(www));
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space)||Input.GetMouseButtonDown(0))
        {
            StartCoroutine(WaitForRequest(www));
        }
	
	}
    IEnumerator WaitForRequest(WWW www)
    {
        print("In wait for request");
        yield return www;

        // check for errors
        if (www.error == null)
        {
            //Do whatever thing
            Debug.Log("WWW Ok!: "+ www.text);
            //textBox.GetComponent<Text>().text = www.text;
            HTMLHolder = www.text;


            //Cut Out the info from the HTML Doc
            int openTable = HTMLHolder.IndexOf("\"rows\":[{");
            int closingTable = HTMLHolder.IndexOf("}]}");
            string tableHolder = HTMLHolder.Substring(openTable, (closingTable - openTable));
            

            //Cut Out the Table's Row from the table holder
            

            int open = tableHolder.IndexOf("\"name\":");
            int closing = tableHolder.IndexOf("\",", open);
            json_Name.GetComponentInChildren<Text>().text = tableHolder.Substring(open + 8, closing - open - 8);

            open = tableHolder.IndexOf("\"Temperature\":");
            closing = tableHolder.IndexOf(",", open);
            json_Temp.GetComponentInChildren<Text>().text = (tableHolder.Substring(open + 14, closing - open - 14))+ "\u00B0 F";

            open = tableHolder.IndexOf("\"isBelowFreezing\":");
            closing = tableHolder.IndexOf(",", open);
            string s_isBelowFreezing= (tableHolder.Substring(open + 18, closing - open - 18));
            json_isBelowFreezing.GetComponentInChildren<Text>().text = s_isBelowFreezing.ToUpper();
            if (s_isBelowFreezing.ToUpper() == "TRUE") json_isBelowFreezing.GetComponent<Image>().color = Color.green;
            else json_isBelowFreezing.GetComponent<Image>().color = Color.red;

            open = tableHolder.IndexOf("\"description\":");
            closing = tableHolder.IndexOf(",", open);
            json_description.GetComponentInChildren<Text>().text = (tableHolder.Substring(open + 14, closing - open - 14));

            open = tableHolder.IndexOf("\"CompressorPercentage\":");
            closing = tableHolder.IndexOf(",", open);
            string stringToIntHolder = (tableHolder.Substring(open + 23, closing - open - 23));
            print(stringToIntHolder);
            barGraph.GetComponent<ScaleBar>().SetBar(int.Parse(stringToIntHolder));

        }
        else
        {
            //Do error thing
            Debug.Log("WWW Error: " + www.error);
        }
    }
}
