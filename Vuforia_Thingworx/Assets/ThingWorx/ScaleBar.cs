﻿using UnityEngine;
using System.Collections;

public class ScaleBar : MonoBehaviour {

    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void SetBar(int scaleINT)
    {
        float barHight = 1.2f * (scaleINT / 100f);
        Vector3 scale = new Vector3(.125f, barHight, .1f);

        gameObject.transform.localScale = scale;
    }
}
